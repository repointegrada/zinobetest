// Bootstrap
import { ButtonToolbar, Form, Button, Toast } from 'react-bootstrap'
// Component
import BaseAmount from './BaseAmount';
// Constants
import * as Constants from '../shared/const/Constants'
// Formik
import { Formik } from 'formik';
// React
import React, { useState, useEffect } from 'react'
// Service
import { AmountRequestService } from '../shared/services/AmountRequestService'
import { UserService } from '../shared/services/UserService';
// Yup
import * as yup from 'yup'

const AmountRequest = props => {

    const paramURL = props.match.params.id
    const MaxAmount = Constants.MaxAmount
    const MinAmount = Constants.MinAmount
    const [show, setShow] = useState(false);
    const [answerValue, setAnswerValue] = useState()
    const [objectuser, setObjectUser] = useState()

    useEffect(() => {
        getUserById()
    }, []);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    // Método para traer un usuario por su ID
    const getUserById = () => {
        UserService.getUserById(paramURL).then(requestResult => {
            if (requestResult) {
                const user = requestResult.data
                setObjectUser(user)
            }
        })
    }

    // Método que muestra un Toast con la respuesta de la solicitud del crédito
    const showAnswer = () => {
        return (
            <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
                <Toast.Body>{answerValue ? '¡Felicitaciones, su crédito fue aprobado!' : 'Lo sentimos, su crédito fue rechazado'}</Toast.Body>
            </Toast>
        )
    }
    // Validación de formulario
    const validationSchema = yup.object().shape({
        amountRequest: yup.number().required('El campo es requerido').max(MaxAmount).min(MinAmount)
    })

    // Styles
    const container = {
        backgroundColor: '#4682b4cc',
        minHeight: '100vh',
        padding: '10px 10px 0px',
        paddingLeft: '25%',
        paddingRight: '25%',
    }

    const title = {
        fontSize: '30px',
        fontWeight: 'bold',
        justifyContent: 'center',
        display: 'flex'
    }

    const infoAmount = {
        display: 'flex',
        justifyContent: 'space-between',
        paddingTop: '10px'
    }

    const alertInfo = {
        display: 'flex',
        alignItems: 'center'
    }

    const buttons = {
        display: 'flex', justifyContent: 'space-around'
    }

    const containerAround = {
        backgroundColor: 'ghostwhite',
        padding: '12px',
        borderRadius: '3%',
        marginTop: '54px'
    }

    return (
        <Formik
            onSubmit={(values) => {
                // Aleatorio que asigna a la solicitud un true (aprobado) o un false (rechazado)
                const isApproved = Math.random() >= 0.5
                let amountRequest = values.amountRequest

                AmountRequestService.updateCreditStatus(paramURL, objectuser, isApproved, amountRequest).then(resultado => {
                    // Mostrar mensaje que aprobó crédito y redirigir a nueva vista
                    if (resultado.creditStatus) {
                        setAnswerValue(true)
                    }

                    // A los que tengan la propiedad creditStatus como false, no pueden volver a realizar solicitud de crédito. Mostrar mensaje que no aprobó crédito y redirigir a nueva vista
                    if (!resultado.creditStatus) {
                        setAnswerValue(false)
                    }

                }).catch(error => {
                    console.log('error', error);
                });
            }}

            validationSchema={validationSchema}
            initialValues={{
                amountRequest: ''
            }}
        >
            {({
                handleSubmit,
                handleChange,
                values,
                touched,
                isValid,
                errors
            }) => (
                    <div style={container}>
                        <div style={containerAround}>
                            <Form onSubmit={handleSubmit} >
                                <span style={title} >Monto a solicitar</span>
                                <div style={infoAmount}>
                                    <span style={alertInfo}>Ingresa el valor a solicitar en tu préstamo, recuerda que la cantidad mínima es ${MinAmount} y la máxima es ${MaxAmount}</span>
                                    <BaseAmount />
                                </div>

                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Monto a solicitar</Form.Label>
                                    <Form.Control
                                        touched={touched}
                                        isValid={isValid}
                                        errors={errors}
                                        value={values.amountRequest}
                                        onChange={handleChange}
                                        name="amountRequest"
                                        placeholder="Ingresa monto a solicitar" />
                                </Form.Group>
                                <div style={buttons}>
                                    <ButtonToolbar>
                                        <Button variant="primary" size="lg" type="submit" onClick={() => setShow(true)}>
                                            Solicitar
                                </Button>

                                        <Button variant="secondary" size="lg" onClick={() => props.history.push({ pathname: '/home/' + paramURL })}>
                                            Ir al home
                                </Button>
                                    </ButtonToolbar>
                                </div>
                                {showAnswer()}
                            </Form>
                        </div>
                    </div>
                )}
        </Formik >
    )
}

export default AmountRequest
