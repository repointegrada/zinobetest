import React, { useEffect, useState } from 'react'
import { Nav } from 'react-bootstrap'
import Logo from '../assets/zinobe.jpeg'
import { UserService } from '../shared/services/UserService'
import BaseAmount from './BaseAmount'
import { NavDropdown } from 'react-bootstrap'

const Home = props => {
    const paramURL = props.match.params.id
    const [objectUser, setObjectUser] = useState({})

    useEffect(() => {
        getUserById()
    }, []);

    // Método para traer un usuario por su ID
    const getUserById = () => {
        UserService.getUserById(paramURL).then(requestResult => {
            if (requestResult) {
                const user = requestResult.data[0]
                setObjectUser(user)
            }
        })
    }

    // Styles
    const container = {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row'
    }
    const containerAround = {
        backgroundColor: '#2bb04a', height: '100vh'
    }
    const colorText = {
        color: 'black'
    }
    const textContainer = {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        marginLeft: '20px'
    }
    const title = {
        justifyContent: 'center',
        display: 'flex',
        fontWeight: 'bold',
        fontSize: '30px'
    }

    return (
        <>
            <div style={container}>
                <div style={containerAround}>
                    <Nav defaultActiveKey="/home" className="flex-column" >
                        <Nav.Link href="#"><img src={Logo} alt="Logo" /></Nav.Link>
                        <Nav.Link href="https://www.zinobe.com/inicio" eventKey="link-2" style={colorText}>Zinobe</Nav.Link>
                        <Nav.Link href={`/amount-request/${paramURL}`} eventKey="link-1" style={colorText} disabled={!objectUser.creditStatus}>Solicitar crédito</Nav.Link>
                        <NavDropdown title="Lista de administrador" id="nav-dropdown" style={colorText}>
                            <NavDropdown.Item eventKey="4.1" href="/registered-users">Usuarios registrados</NavDropdown.Item>
                            <NavDropdown.Item eventKey="4.2" href="/users-info/rejected-users">Usuarios rechazados</NavDropdown.Item>
                            <NavDropdown.Item eventKey="4.3" href="/users-info/approved-users">Usuarios aprobados</NavDropdown.Item>
                            <NavDropdown.Item eventKey="4.4" href="/users-info/pending-users">Usuarios pendientes de pago</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </div>

                <div style={textContainer}>
                    <p style={title}>¡Bienvenid@ {objectUser.name} a Zinobe!</p>
                    <span>Somos una fintech colombiana líder desarrollando productos financieros de clase mundial. Buscamos impulsar la inclusión financiera. Empoderar a miles de personas y mipymes. Llegar a los desatendidos e insatisfechos. Construir confianza con transparencia. Y reinventar los modelos de riesgo para hacerlos más flexibles e inclusivos. ¿El resultado? 1M+ de créditos desembolsados en los 32 departamentos de Colombia.</span>
                    <BaseAmount />
                </div>
            </div>
        </>
    )
}

export default Home
