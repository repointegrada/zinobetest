// Constants
import * as Constants from '../shared/const/Constants'
// React
import React, { useEffect, useState } from 'react'
// Service
import { UserService } from '../shared/services/UserService'

const BaseAmount = () => {
    const [InfoByStatus, setInfoByStatus] = useState([]);

    useEffect(() => {
        // Sumatoria para los préstamos aprobados
        UserService.getUserByStatus('creditStatus', false).then(requestResult => {
            if (requestResult) {
                const userByStatus = requestResult.data
                setInfoByStatus(userByStatus)
            }
        })
    }, [])

    // Styles
    const infoBaseAmount = {
        display: 'flex', flexDirection: 'column', padding: '10px', width: '450px', height: '10%', justifyContent: 'center', borderStyle: 'dotted'
    }

    const bold = {
        fontWeight: 'bold'
    }

    // Monto máximo disponible en el banco
    const MaxAmount = Constants.MaxAmount
    const datos = InfoByStatus.map((data) => data.solicitedValue)

    if (!datos) return
    // Sumar cada uno de los valores de los créditos aprobados de los usuarios
    let sum = a => a.reduce((x, y) => x + y, 0);
    let totalAmount = sum(datos.map(x => Number(x)));
    // Nueva capacidad del banco
    let capacity = MaxAmount - totalAmount

    return (
        <div style={infoBaseAmount}>
            {/* 1. Mostrar info con el monto base del banco */}
            <span style={bold}> Monto base del banco: </span > {MaxAmount}
            {/* 2. Realizar operación */}
            <span style={bold}> Monto base del banco - préstamos aprobados = </span >{capacity}
        </div >
    )
}

export default BaseAmount
