// Bootstrap
import { Form, Button, ButtonToolbar } from 'react-bootstrap'
// Formik
import { Formik } from 'formik';
// React
import React from 'react'
import { withRouter } from 'react-router-dom'
// Service
import { UserService } from '../shared/services/UserService'

const Users = props => {

    // Styles
    const container = {
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: '25%',
        paddingRight: '25%',
        backgroundColor: '#4682b4cc',
        minHeight: '100vh'
    }

    const containerAround = {
        backgroundColor: 'ghostwhite',
        padding: '12px',
        borderRadius: '3%',
        marginTop: '54px'
    }

    const title = {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '24px',
        fontWeight: 'bold'
    }

    return (
        <Formik
            onSubmit={(values) => {
                UserService.createUser(values).then(requestResult => {
                    if (requestResult) {
                        const id = requestResult.data.id
                        return props.history.push({ pathname: '/amount-request/' + id })
                    }
                })
            }}

            initialValues={{
                name: '',
                email: '',
                documentNumber: '',
                solicitedValue: 0,
                creditStatus: false,
                pendingPayment: false
            }
            }
        >
            {({
                handleSubmit,
                handleChange,
                values,
            }) => (
                    <div style={container}>
                        <div style={containerAround}>
                            <p style={title}>Crear cuenta</p>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Nombre</Form.Label>
                                    <Form.Control value={values.name} onChange={handleChange} name="name" type="text" placeholder="Ingresa tu nombre" />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Correo electrónico</Form.Label>
                                    <Form.Control value={values.email} onChange={handleChange} name="email" type="email" placeholder="Ingresa tu email" />
                                </Form.Group>

                                <Form.Group controlId="formBasicDocument">
                                    <Form.Label>Número de documento</Form.Label>
                                    <Form.Control value={values.documentNumber} onChange={handleChange} name="documentNumber" type="text" placeholder="Ingresa tu número de documento" />
                                </Form.Group>

                                <Form.Group controlId="formBasicSolicitedValor">
                                    <Form.Label>Monto solicitado</Form.Label>
                                    <Form.Control value={values.solicitedValue} onChange={handleChange} name="solicitedValue" type="text" placeholder="Ingresa tu valor solicitado" />
                                </Form.Group>
                                <ButtonToolbar>
                                    <Button variant="primary" type="submit" block>
                                        Crear
                                </Button>
                                </ButtonToolbar>
                            </Form>
                        </div>
                    </div>
                )}
        </Formik >
    )
}

export default withRouter(Users)
