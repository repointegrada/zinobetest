import React, { useState, useEffect } from 'react'
import { UserService } from '../shared/services/UserService';
import { Table, Button } from 'react-bootstrap';


const RegisteredUsers = props => {

    const [AllUsers, setAllUsers] = useState([])

    useEffect(() => {
        getAllUsers()
    }, [])

    // Método para consumir servicio que trae todos los usuarios
    const getAllUsers = () => {
        UserService.getAllUsers().then(requestResult => {
            if (requestResult) {
                const AllUsers = requestResult.data
                setAllUsers(AllUsers)
            }
        })
    }

    // Método para pintar la iteración de los usuarios
    const renderUsers = (users, index) => {
        return (
            <tr key={index}>
                <td>{users.name}</td>
                <td>{users.email}</td>
                <td>{users.documentNumber}</td>
                <td>{users.creditStatus}</td>
                <td>{users.pendingPayment}</td>
            </tr>
        )
    }

    // Styles 
    const dynamicTitle = {
        display: 'flex',
        justifyContent: 'center',
        fontSize: '54px'
    }

    const tableMargin = {
        paddingLeft: '10%',
        paddingRight: '10%'
    }

    return (
        <>
            <div>
                <span style={dynamicTitle}>Usuarios registrados en el sistema</span>
            </div>

            <div style={tableMargin}>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Email</th>
                            <th>Número de documento</th>
                            <th>Estado de crédito</th>
                            <th>Pendiente de pago</th>
                        </tr>
                    </thead>
                    <tbody>
                        {AllUsers.map(renderUsers)}
                    </tbody>
                </Table>
                <Button variant="primary" onClick={() => props.history.goBack()}> Volver</Button>
            </div>
        </>
    )
}

export default RegisteredUsers
