import React, { useState, useEffect } from 'react'
import { Table, Button } from 'react-bootstrap';
import { UserService } from '../shared/services/UserService';

const UsersInfo = props => {

    
    console.log('props en usuarios registrados ', props);

    const paramURL = props.match.params.id
    const [InfoByStatus, setInfoByStatus] = useState([]);

    useEffect(() => {
        getUserByStatus()
    }, [paramURL])


    // Validar que url llega para realizar validaciones y enviar state y value correspondiente
    // Consumir servicio y enviarle datos
    const getUserByStatus = () => {
        if (paramURL === 'rejected-users') {
            UserService.getUserByStatus('creditStatus', false).then(requestResult => {
                if (requestResult) {
                    const userByStatus = requestResult.data
                    setInfoByStatus(userByStatus)
                }
            })
        }
        if (paramURL === 'approved-users') {
            UserService.getUserByStatus('creditStatus', true).then(requestResult => {
                if (requestResult) {
                    const userByStatus = requestResult.data
                    setInfoByStatus(userByStatus)
                }
            })
        }
        if (paramURL === 'pending-users') {
            UserService.getUserByStatus('pendingPayment', true).then(requestResult => {
                if (requestResult) {
                    const userByStatus = requestResult.data
                    setInfoByStatus(userByStatus)
                }
            })
        }
    }

    // Styles 
    const dynamicTitle = {
        display: 'flex',
        justifyContent: 'center',
        fontSize: '54px',
        paddingLeft: '24px'
    }

    const tableMargen = {
        paddingLeft: '10%',
        paddingRight: '10%'
    }

    // Método para pintar la iteración de los usuarios por status
    const renderUsersByStatus = (users, index) => {
        return (
            <tr key={index}>
                <td>{users.name}</td>
                <td>{users.email}</td>
                <td>{users.documentNumber}</td>
                <td>{users.solicitedValue}</td>
            </tr>
        )
    }

    return (
        <>
            <span style={dynamicTitle}>{paramURL === 'rejected-users' ? "Usuarios rechazados" : paramURL === 'approved-users' ? "Usuarios aprobados" : "Usuarios pendientes por pagar"}</span>
            <div style={tableMargen}>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Email</th>
                            <th>Número de documento</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        {InfoByStatus.map(renderUsersByStatus)}
                    </tbody>
                </Table>
                <Button variant="primary" onClick={() => props.history.goBack()}> Volver</Button>

            </div>
        </>
    )
}

export default UsersInfo
