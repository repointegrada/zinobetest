// Axios
import axios from 'axios';
// url base de usuarios
const baseUrl = 'http://localhost:4000/users'

//Metodo para crear un nuevo usuario
async function createUser(values) {
    return await axios.post(baseUrl, {
        ...values
    }).then(resp => {
        return resp
    }).catch(error => {
        console.log('error', error);
    });
}

// Método para traer todos los usuarios
async function getAllUsers() {
    return await axios.get(baseUrl).then(resp => {
        return resp
    }).catch(error => {
        console.log('error', error);
    });
}

// Método para traer un usuario por ID
async function getUserById(id) {
    return await axios.get(`${baseUrl}?id=${id}`).then(resp => {
        return resp
    }).catch(error => {
        console.log('error', error);
    });
}

// Método para traer usuario por un estado (rechazados, aprobados o pendiente por pagar)
async function getUserByStatus(state, value) {
    const ruta = `${baseUrl}?${state}=${value}`
    return await axios.get(ruta).then(resp => {
        return resp
    }).catch(error => {
        console.log('error', error);
    });
}

export const UserService = {
    createUser,
    getAllUsers,
    getUserById,
    getUserByStatus
}