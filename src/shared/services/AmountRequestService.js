// Axios
import axios from 'axios';
// url base de usuarios
const baseUrl = 'http://localhost:4000/users/'

async function updateCreditStatus(paramURL, objectuser, isApproved, solicitedValue) {

    const position = objectuser[0]

    let data = {
        solicitedValue: solicitedValue,
        creditStatus: isApproved
    }

    const returnedTarget = Object.assign(position, data);

    return await axios.put(`${baseUrl}${paramURL}`,
        returnedTarget
    ).then(resp => {
        return resp.data
    }).catch(error => {
        console.log(error);
    });
}

export const AmountRequestService = {
    updateCreditStatus
}