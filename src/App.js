import React from 'react';
import Router from '../src/Administrator/Router'

function App() {
  // Referenciar router (Rutas de proyecto)
  // 1. Crear usuarios
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
