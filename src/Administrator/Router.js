// React
import React, { Suspense } from 'react';
// Router
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import { BeatLoader } from 'react-spinners'


// Lazy load
const Home = React.lazy(() => import('../components/Home'));
const AmountRequest = React.lazy(() => import('../components/AmountRequest'));
const RegisteredUsers = React.lazy(() => import('../components/RegisteredUsers'));
const Users = React.lazy(() => import('../components/Users'));
const UsersInfo = React.lazy(() => import('../components/UsersInfo'));

// A los componentes que necesitan props, se hace destrucutración y se le envía la prop necesaria
const Router = () => {
    return (
        <BrowserRouter>
            <Suspense fallback={<BeatLoader />}>
                <Switch>

                    <Route path="/registered-users" render={({
                        history
                    }) => (
                            <RegisteredUsers history={history} />
                        )}
                    />

                    <Route path="/create-users">
                        <Users />
                    </Route>

                    <Route
                        exact
                        path="/home/:id"
                        render={({
                            match
                        }) => (
                                <Home match={match} />
                            )}
                    />

                    <Route path="/amount-request/:id"
                        render={({
                            history,
                            match
                        }) => (
                                <AmountRequest history={history} match={match} />
                            )}
                    />

                    <Route path="/users-info/:id" render={({
                        history,
                        match
                    }) => (
                            <UsersInfo match={match} history={history} />
                        )}
                    />
                    <Redirect from='/' to='/create-users' />
                </Switch>
            </Suspense>
        </BrowserRouter>
    )
}

export default Router